# 需求
1、多次向工作内存中插入 Fact 对象，看在不同的 equality modes 下，工作内存中会存在几个对象。  
2、equalsBehavior在identity和equality下向工作内存中插入对象的不同结果

# 结论
针对如下代码，看看在不同equality modes下的行为

```java
 Person p1 = new Person("zhangsan", 20, "湖北罗田");
 Person p2 = new Person("zhangsan", 20, "湖北黄冈罗田");

 FactHandle factHandle1 = kieSession.insert(p1);
 FactHandle factHandle2 = kieSession.insert(p2);
 FactHandle factHandle3 = kieSession.insert(p2);
```
`Person`对象的`hashcode和equals`方法进行重写了，根据构造方法的前2个参数。

## 1、identity模式下
`factHandle1 != factHandle2` 因为p1和p2是2个不同的对象。  
`factHandle2 == factHandle3` 因为是p2重复加入工作内存，这个时候工作内存中已经存在了，所以返回之前关联的`FactHandle`

## 2、equality模式下
`factHandle1 == factHandle2 == factHandle3` 因为这种模式下，是需要根据对象的`equals`和`hashcode`方法进行比较，而`Person`对象重写了这2个方法，所以返回的是同一个。

# 博客地址
[https://blog.csdn.net/fu_huo_1993/article/details/124766913](https://blog.csdn.net/fu_huo_1993/article/details/124766913)