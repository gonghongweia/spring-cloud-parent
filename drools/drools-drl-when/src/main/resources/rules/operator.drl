package  rules

import com.huan.drools.com.huan.drools.fact.Person
import com.huan.drools.BMWCar

// 使用 `.()` 运算符将属性访问器分组到嵌套对象
rule ".()"
    when
        Person(age < 20 , car.(name == "宝马" , color == null )) // 等价与 Person(age < 20 , car.name == "宝马" , car.color == null )
    then
        System.out.println(".()");
end

// 类型转换 在嵌套模式中，我们可以使用 `<type>#<subtype>`语法强制转换为子类型并使父类型的 getter 用于子类型。
rule "#"
    when
        Person(car#BMWCar.brand == "BMW")
    then
        System.out.println("#");
end

// 嵌套属性null安全
rule "!."
    when
        Person(car!.name == "宝马") // 如果此时 car 为null，则使用 car!.name 是不会报错的
    then
        System.out.println("!.");
end

// [] - list操作
rule "[]-list"
    when
        Person(hobbyList[0] == "打篮球")
    then
        System.out.println("[]-list");
end

// [] - map操作
rule "[]-map"
    when
        Person(map["key1"] == "value1")
    then
        System.out.println("[]-map");
end

// 和java中类似的操作符
rule "<, <=, >, >=，==, !=，&&，||"
    when
//        Person(age ((> 30 && < 40) || (>= 18 && <= 25)) && car != null && registerDate < '2022-12-12 12:12:12')
        Person(age >= 18 && age <= 25)
    then
        System.out.println("<, <=, >, >=，==, !=，&&，||");
end

// matches or not matches 正则匹配
rule "matches or not matches"
    when
        Person(name matches hobbyList[2] && car.name not matches "^奥迪") // 正则表达式可以是动态来的
    then
        System.out.println("matches or not matches");
end

// contains, not contains 集合或字符串是否包含或不包含什么
/**
    1、验证一个`Array`或`Collection`是否包含某个指定字段的值（可以是<sub>`常量`</sub>也可以是<sub>`变量`</sub>）。
    2、也可以是`String`类型的字段是否包含某个值（可以是<sub>`常量`</sub>也可以是<sub>`变量`</sub>）。
    3、为了向后兼容，excludes运算符和not contains的作用一致。
*/
rule "contains, not contains"
    when
        Person(
            hobbyList contains "打篮球" && hobbyList not contains "打橄榄球" && hobbyList not contains name &&
            name contains "张" && name not contains car.name
        )
    then
        System.out.println("contains, not contains");
end

// 验证某个字段是否是`Array`或`Collection`的一员。`Array`或`Collection`必须是可变的。
rule "memberOf, not memberOf"
    when
        Person("打篮球" memberOf hobbyList && "篮球" not memberOf hobbyList && name not memberOf hobbyList)
    then
        System.out.println("memberOf, not memberOf");
end

/*
1. 验证指定的字符串是以什么开头`str[startsWith]`。
2. 验证指定的字符串是以什么结尾`str[endsWith]`。
3. 验证指定字符串的长度`str[length]`。
4. 查看这个类`org.drools.core.base.evaluators.StrEvaluatorDefinition`
*/
rule "str"
    when
        Person(
            name str[startsWith] "张" && name str[endsWith] "三" &&
            name str[length] 2 && "张三" str[startsWith] "张"
        )
    then
        System.out.println("str");
end

// 判断某个元素是否在一组元素中
rule "in not in"
    when
        Person(
            $name: name &&
            name in ($name, "李四") &&
            "打篮球"  in ("打篮球", "踢足球") &&
            car.name not in ("打篮球", $name)
        )
    then
        System.out.println("in not in");
end