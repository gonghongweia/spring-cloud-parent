package com.huan.drools;

import lombok.Getter;
import lombok.Setter;

import java.util.regex.Pattern;

/**
 * @author huan.fu
 * @date 2022/5/23 - 17:43
 */
@Getter
@Setter
public class BMWCar extends Car {
    private String brand;

    public BMWCar(String name, String brand) {
        this.brand = brand;
        setName(name);
    }

    public static void main(String[] args) {
        System.out.println(Pattern.compile("^宝马").matcher("宝马").matches());
    }
}
