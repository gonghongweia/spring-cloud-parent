package com.huan.drools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * drools 规则动态加载
 */
@SpringBootApplication
public class DroolsLoadRuleFromDBApplication {
    public static void main(String[] args) {
        SpringApplication.run(DroolsLoadRuleFromDBApplication.class, args);
    }
}
