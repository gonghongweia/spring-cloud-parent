package com.huan.drools.autofocus;

import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * 测试规则分组-在规则文件中默认激活分组
 */
public class DroolsAutoFocusApplication {
    public static void main(String[] args) {
        // 设置日期格式，否则可能会报错(Wrong date-effective value: Invalid date input format: [2022-05-18 10:54:26] it should follow: [d-MMM-yyyy]]])
        System.setProperty("drools.dateformat", "yyyy-MM-dd HH:mm:ss");
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("rule-attributes-ksession");

        // 只匹配规则名称是已 auto_focus_ 开头的规则，忽略其余的规则
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("auto_focus_"));
        kieSession.dispose();
    }
}
