package com.huan.drools.lockonactive;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * 一个简单的实体类
 *
 * @author huan.fu
 * @date 2022/5/18 - 14:34
 */
@Getter
@Setter
@AllArgsConstructor
public class Person {
    private String name;
    private Integer age;
}
