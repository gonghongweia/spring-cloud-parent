package com.huan.drools;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 学生类
 *
 * @author huan.fu
 * @date 2022/5/29 - 13:45
 */
@Getter
@Setter
@ToString
public class Student {

    private String name;
    // 分数只能在 0-100 之间
    private Integer score;

    public Student(String name, Integer score) {
        this.name = name;
        if (null == score || score < 0 || score > 100) {
            throw new RuntimeException("分数只能在0-100之间");
        }
        this.score = score;
    }
}
