# 实现功能

1、决策表的简单使用。  
2、决策表的语法，在博客中查看。  

# 博客地址

[https://blog.csdn.net/fu_huo_1993/article/details/125035986](https://blog.csdn.net/fu_huo_1993/article/details/125035986)

# 参考链接

1、[https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#decision-tables-con_decision-tables](https://docs.drools.org/7.69.0.Final/drools-docs/html_single/index.html#decision-tables-con_decision-tables)



