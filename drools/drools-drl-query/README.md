# 实现功能

1、无参数query的使用  
2、有参数query的使用  
3、java代码中`openLiveQuery`的使用  
4、rule中使用query

drl中的query是为了获取工作内存中的对象的值

# 注意事项

1、不要向同一RuleBase 的不同包添加相同名称的query  

2、如果出现了如下异常`Query's must use positional or bindings, not field constraints:
"张" : [Rule name='rule_person_name_starts_with']`，这个是因为我们在`rule`中调用`query`时，参数没有以`;`结尾。正确用法`personNameStartsWith("张";)`  

3、`?personNameStartsWith("张";) 和 personNameStartsWith("张";)`是不一样的。The `?` symbol means the query is pull only, once the results are returned you will not receive further results as the underlying data changes

# 博客地址
[https://blog.csdn.net/fu_huo_1993/article/details/124792736](https://blog.csdn.net/fu_huo_1993/article/details/124792736)