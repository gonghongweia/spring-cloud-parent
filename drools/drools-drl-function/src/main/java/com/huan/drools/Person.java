package com.huan.drools;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author huan.fu
 * @date 2022/5/14 - 19:25
 */
@Data
@AllArgsConstructor
public class Person {
    private String name;
    private Integer age;
}
