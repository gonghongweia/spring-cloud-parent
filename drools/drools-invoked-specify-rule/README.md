# 实现功能

1、执行具体指定的规则。  
通过`AgendaFilter`来实现，我们知道模式匹配过滤，所有激活的规则会放入到`Agenda`中，我们通过`AgendaFilter`进行过滤后，就
可以实现调用我们自己想要的规则。  
通过`entry-point`来实现，这个也可以实现。`drl`文件中的`rule`在模式匹配的时候，来源的数据是通过`enty-point`来的，进行匹配。


# 博客地址

[https://blog.csdn.net/fu_huo_1993/article/details/124949785](https://blog.csdn.net/fu_huo_1993/article/details/124949785)

# 参考链接
