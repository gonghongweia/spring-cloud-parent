package com.huan.drools;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Api Fact
 *
 * @author huan.fu
 * @date 2022/5/24 - 16:22
 */
@Getter
@Setter
@AllArgsConstructor
public class Api {

    private String api;
    private Integer invokedCnt;

}
