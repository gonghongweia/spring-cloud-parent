package com.huan.drools.config;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * drools 配置类
 *
 * @author huan.fu
 * @date 2022/5/25 - 17:26
 */
@Configuration
public class DroolsConfig {

    @Bean
    public KieContainer kieContainer() {
        KieServices kieServices = KieServices.get();
        return kieServices.getKieClasspathContainer();
    }
}
