package com.huan.study.sharding.controller;

import com.huan.study.sharding.entity.Activity;
import com.huan.study.sharding.entity.Customer;
import com.huan.study.sharding.mappers.ActivityMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author huan.fu 2021/5/18 - 上午10:51
 */
@RestController
public class ActivityController {

    @Resource
    private ActivityMapper activityMapper;

    @GetMapping("addActivity")
    public String addCustomer(@RequestParam("activityName") String activityName) {
        int result = activityMapper.addActivity(activityName);
        return "添加结果: " + result;
    }

    @GetMapping("findActivity")
    public Activity findCustomer(@RequestParam("id") Long id) {
        return activityMapper.findActivity(id);
    }
}
