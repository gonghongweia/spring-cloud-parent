package com.huan.study.sharding.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author huan.fu 2021/5/24 - 下午5:27
 */
public class CustomerOrder {
    private Long id;
    private BigDecimal orderId;
    private Long customerId;
    private Long sallerId;
    private String productName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getOrderId() {
        return orderId;
    }

    public void setOrderId(BigDecimal orderId) {
        this.orderId = orderId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getSallerId() {
        return sallerId;
    }

    public void setSallerId(Long sallerId) {
        this.sallerId = sallerId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
