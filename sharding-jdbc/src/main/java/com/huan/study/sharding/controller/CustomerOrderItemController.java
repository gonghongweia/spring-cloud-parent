package com.huan.study.sharding.controller;

import com.huan.study.sharding.entity.CustomerOrderItem;
import com.huan.study.sharding.mappers.CustomerOrderItemMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 订单
 *
 * @author huan.fu 2021/5/25 - 下午1:40
 */
@RestController
public class CustomerOrderItemController {

    @Resource
    private CustomerOrderItemMapper customerOrderItemMapper;

    @GetMapping("findOrderItem")
    public CustomerOrderItem findOrder(@RequestParam("orderId") BigDecimal orderId) {
        return customerOrderItemMapper.findOrderItem(orderId);
    }

    @GetMapping("findCustomerOrderItems")
    public List<CustomerOrderItem> findCustomerOrders(@RequestParam("customerId") Long customerId) {
        return customerOrderItemMapper.findCustomerOrderItems(customerId);
    }

    @GetMapping("findOrderItemDetail")
    public Map<String, Object> findOrderItemDetail(@RequestParam("orderId") BigDecimal orderId) {
        return customerOrderItemMapper.findOrderItemDetail(orderId);
    }
}
