package com.huan.study.sharding.entity;


/**
 * @author huan.fu 2021/5/18 - 上午10:42
 */
public class Activity {

    private Long id;

    private String activityName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }
}
