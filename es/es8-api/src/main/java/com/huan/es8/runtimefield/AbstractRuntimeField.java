package com.huan.es8.runtimefield;

import co.elastic.clients.elasticsearch._types.mapping.*;
import com.huan.es8.AbstractEs8Api;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.io.IOException;
import java.util.Arrays;

/**
 * 构建runtime field需要用到的索引和数据
 *
 * @author huan.fu
 * @date 2023/2/1 - 21:42
 */
public class AbstractRuntimeField extends AbstractEs8Api {

    public static final String INDEX_NAME = "index_script_fields";

    @BeforeAll
    public void createIndex() throws IOException {
        client.indices()
                .create(indexRequest ->
                        indexRequest.index(INDEX_NAME)
                                .mappings(mappings ->
                                        mappings.properties("lineId", new Property(new KeywordProperty.Builder().build()))
                                                .properties("lineName", new Property(new TextProperty.Builder().build()))
                                )
                );
        bulk(INDEX_NAME, Arrays.asList(
                "{\"lineId\":\"line-01\",\"lineName\":\"线路A\"}",
                "{\"lineId\":\"line-01\",\"lineName\":\"线路A\"}",
                "{\"lineId\":\"line-02\",\"lineName\":\"线路C\"}"
        ));
    }


    @AfterAll
    public void deleteIndex() throws IOException {
        deleteIndex(INDEX_NAME);
    }
}
