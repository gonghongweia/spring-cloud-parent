package com.huan.hadoop.mr;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/**
 * 分组： 订单编号相同说明是同一组，否则是不同的组
 *
 * @author huan.fu
 * @date 2023/7/13 - 14:30
 */
public class TopNGroupingComparator extends WritableComparator {

    public TopNGroupingComparator() {
        // 第二个参数为true: 表示可以通过反射创建实例
        super(OrderVo.class, true);
    }

    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        // 订单编号 相同说明是同一个对象，否则是不同的对象
        return ((OrderVo) a).getOrderId() == ((OrderVo) b).getOrderId() ? 0 : 1;
    }
}
