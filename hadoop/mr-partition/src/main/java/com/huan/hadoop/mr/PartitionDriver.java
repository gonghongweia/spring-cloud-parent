package com.huan.hadoop.mr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/**
 * 驱动类
 * <p>
 * 需求： 统计每个手机的上行流量和下行流量， 且按照`上行流量倒序`排序。（具体看README.md文件）
 *
 * @author huan.fu
 * @date 2023/7/10 - 19:46
 */
public class PartitionDriver extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        // 构建配置对象
        Configuration configuration = new Configuration();
        // 使用 ToolRunner 提交程序
        int status = ToolRunner.run(configuration, new PartitionDriver(), args);
        // 退出程序
        System.exit(status);
    }

    @Override
    public int run(String[] args) throws Exception {
        // 构建Job对象实例 参数（配置对象，Job对象名称）
        Job job = Job.getInstance(getConf(), PartitionDriver.class.getName());
        // 设置mr程序运行的主类
        job.setJarByClass(PartitionDriver.class);
        // 设置mr程序运行的 mapper类型和reduce类型
        job.setMapperClass(PartitionMapper.class);
        job.setReducerClass(PartitionReducer.class);
        // 指定mapper阶段输出的kv数据类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        // 指定reduce阶段输出的kv数据类型，业务mr程序输出的最终类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(NullWritable.class);

        // 设置分区类
        job.setPartitionerClass(WebSitePartitioner.class);
        // 设置reduceTask的个数， reduceTask的个数需要和分区数一致。
        job.setNumReduceTasks(2);

        // 配置本例子中的输入数据路径和输出数据路径，默认输入输出组件为： TextInputFormat和TextOutputFormat
        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        // 先删除输出目录（方便本地测试）
        FileSystem.get(this.getConf()).delete(new Path(args[1]), true);
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
