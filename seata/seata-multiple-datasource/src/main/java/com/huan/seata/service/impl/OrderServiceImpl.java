package com.huan.seata.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.huan.seata.entity.OrderDO;
import com.huan.seata.mapper.OrderMapper;
import com.huan.seata.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author huan.fu 2021/9/27 - 下午2:34
 */
@Service
@AllArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {
    
    private final OrderMapper orderMapper;
    
    @Override
    @Transactional(rollbackFor = Exception.class)
    @DS("order")
    public void createOrder(Integer accountId, Long amount) {
        log.info("为账户accountId:[{}]创建订单.", accountId);
        OrderDO order = new OrderDO();
        order.setAccountId(accountId);
        order.setAmount(amount);
        order.setOrderTime(new Timestamp(new Date().getTime()));
        orderMapper.createOrder(order);
    }
}
