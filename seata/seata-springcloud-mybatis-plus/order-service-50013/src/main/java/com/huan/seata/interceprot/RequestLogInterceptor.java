package com.huan.seata.interceprot;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author huan.fu 2021/9/26 - 下午5:35
 */
@Slf4j
public class RequestLogInterceptor implements HandlerInterceptor {
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        log.info("当前请求的url为:[{}]", request.getRequestURI());
        return true;
    }
}
