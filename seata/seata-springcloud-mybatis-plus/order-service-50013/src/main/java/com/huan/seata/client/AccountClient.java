package com.huan.seata.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author huan.fu 2021/9/26 - 下午5:45
 */
@FeignClient(name = "account-service")
public interface AccountClient {
    
    @GetMapping("account/debit")
    String debit(@RequestParam("id") Integer id, @RequestParam("amount") Long amount);
}
