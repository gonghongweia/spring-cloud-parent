package com.huan.seata;

import io.seata.rm.datasource.DataSourceProxy;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

/**
 * @author huan.fu 2021/9/16 - 下午2:54
 */
@SpringBootApplication
@MapperScan("com.huan.seata.mapper")
public class OrderService50002Application {
    
    @Autowired
    private DataSource dataSource;
    
    @PostConstruct
    public void aa(){
        System.out.println(dataSource.getClass());
        System.out.println(dataSource instanceof DataSourceProxy);
        System.out.println("===========================");
    }
    
    public static void main(String[] args) {
        SpringApplication.run(OrderService50002Application.class);
    }
}
