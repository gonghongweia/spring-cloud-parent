# 注意事项

1、seata 的 AT 模式，数据源必须是 DataSourceProxy,不然分布式事物可能无法实现
2、表中必须一个单一主键
3、业务侧数据库中需要存在一张 undo_log 表
4、数据库 datetime 类型修改成 timestamp 类型 seata1.4的 bug
    https://www.cnblogs.com/luckyang/p/14862630.html
5、数据源需要使用 druid
6、自己写拦截器传递 xid

account-server-50001
    使用 druid 数据源
order-service-50002
    使用 Hikari 数据源，关闭默认的数据源代理，自己配置

service:
    # 事务群组
    vgroup-mapping:
      tx_order_service_group: default
发现配置了没有什么用。 可能是seata.tx-service-group属性生效了