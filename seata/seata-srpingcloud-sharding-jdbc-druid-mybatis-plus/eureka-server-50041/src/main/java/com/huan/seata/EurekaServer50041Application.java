package com.huan.seata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 服务发现启动类
 *
 * @author huan.fu 2021/10/12 - 上午11:03
 */
@SpringBootApplication
@EnableEurekaServer
public class EurekaServer50041Application {
    
    public static void main(String[] args) {
        SpringApplication.run(EurekaServer50041Application.class);
    }
}
