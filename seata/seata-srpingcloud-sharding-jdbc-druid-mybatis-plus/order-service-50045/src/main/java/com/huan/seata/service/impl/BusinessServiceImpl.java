package com.huan.seata.service.impl;

import com.huan.seata.client.AccountClient;
import com.huan.seata.service.BusinessService;
import com.huan.seata.service.OrderService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author huan.fu 2021/9/16 - 下午2:35
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class BusinessServiceImpl implements BusinessService {
    
    private final OrderService orderService;
    private final AccountClient accountClient;
    
    @Override
    // 和 sharding-jdbc 整合后，代码中不要出现 @GlobalTransactional 注解，且也不能和 @ShardingTransactionType注解混用
    // @GlobalTransactional(rollbackFor = Exception.class)
    // @ShardingTransactionType(TransactionType.BASE)
    // @Transactional(rollbackFor = Exception.class)
    @GlobalTransactional(rollbackFor = Exception.class,timeoutMills = 6000000)
    public void createAccountOrder(Integer accountId, Long amount, boolean hasException) {
        System.out.println("xid:" + RootContext.getXID());
        // 1、远程扣减账户余额
        String result = accountClient.debit(accountId, amount);
        log.info("远程扣减库存结果:[{}]", result);
        
        // 2、下订单
        orderService.createOrder(accountId, amount);
        
        if (hasException) {
            throw new RuntimeException("发生了异常，分布式事物需要回滚");
        }
    }
}
