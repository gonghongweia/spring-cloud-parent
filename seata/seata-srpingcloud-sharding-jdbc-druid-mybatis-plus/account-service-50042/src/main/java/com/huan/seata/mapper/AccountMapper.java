package com.huan.seata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huan.seata.entity.AccountDO;

/**
 * @author huan.fu 2021/9/26 - 下午2:01
 */
public interface AccountMapper extends BaseMapper<AccountDO> {
}
