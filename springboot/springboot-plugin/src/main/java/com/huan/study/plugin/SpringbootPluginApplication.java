package com.huan.study.plugin;

import com.huan.study.plugin.sms.SmsPlugin;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.plugin.core.config.EnablePluginRegistries;

@SpringBootApplication
@EnablePluginRegistries(value = {SmsPlugin.class})
public class SpringbootPluginApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(SpringbootPluginApplication.class, args);
    }
}
