package com.huan.springcloud;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 控制器
 *
 * @author huan.fu
 * @date 2024/10/16 - 13:23
 */
@RestController
public class TestController {

    private static final Logger log = LoggerFactory.getLogger(TestController.class);

    @PostMapping("printRequestInfo")
    public String printRequestInfo(@RequestBody Map<String, String> request) {
        log.info(" 获取到的请求体参数为: {}", request);
        return "请求成功";
    }
}