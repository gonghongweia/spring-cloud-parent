package com.huan.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * spring boot 内置的一些小功能
 *
 * @author huan.fu
 * @date 2024/10/16 - 13:19
 */
@SpringBootApplication
public class BuitInApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuitInApplication.class, args);
    }
}
