package com.huan.geodesy;

import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GeodeticCurve;
import org.gavaghan.geodesy.GlobalCoordinates;

/**
 * 计算2个点之间点距离
 *
 * @author huan.fu
 * @date 2024/8/20 - 12:47
 */
public class GeodesyApplication {

    public static void main(String[] args) {
        GeodeticCalculator calculator = new GeodeticCalculator();
        GeodeticCurve curve = calculator.calculateGeodeticCurve(
                Ellipsoid.Sphere,
                new GlobalCoordinates(21.648511, 110.805695),
                new GlobalCoordinates(23.512463, 119.826391)
        );
        double distance = curve.getEllipsoidalDistance();
        System.out.println("distance: " + distance + "米");
    }
}
