package com.huan.xss.core;

import com.huan.xss.config.XssProperties;
import org.jsoup.internal.StringUtil;
import org.springframework.web.util.HtmlUtils;

import java.nio.charset.StandardCharsets;

/**
 * 默认的 xss 清理器
 *
 * @author huan.fu
 */
public class DefaultXssCleaner implements XssCleaner {

	private final XssProperties xssProperties;

	public DefaultXssCleaner(XssProperties xssProperties) {
		this.xssProperties = xssProperties;
	}

	@Override
	public String clean(String bodyHtml) {
		// 1. xss 没开启，直接返回
		if(!xssProperties.isEnabled()){
			return bodyHtml;
		}

		// 2. 为空直接返回
		if (StringUtil.isBlank(bodyHtml)) {
			return bodyHtml;
		}
		// 3. html 转义
		return HtmlUtils.htmlEscape(bodyHtml, StandardCharsets.UTF_8.name());
	}
}
