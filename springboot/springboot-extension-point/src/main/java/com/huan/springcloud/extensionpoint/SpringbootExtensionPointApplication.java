package com.huan.springcloud.extensionpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootExtensionPointApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootExtensionPointApplication.class, args);
	}

}
