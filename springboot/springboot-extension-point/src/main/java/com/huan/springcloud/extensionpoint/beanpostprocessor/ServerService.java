package com.huan.springcloud.extensionpoint.beanpostprocessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * server service
 *
 * @author huan.fu
 * @date 2022/5/30 - 16:07
 */
@Service
public class ServerService implements InitializingBean {
    private static final Logger log = LoggerFactory.getLogger(ServerService.class);

    private String name;

    @Value("${server.port}")
    private Integer serverPort;

    @PostConstruct
    public void postConstruct() {
        log.error("postConstruct");
    }

    @Override
    public void afterPropertiesSet() {
        log.error("afterPropertiesSet");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getServerPort() {
        return serverPort;
    }

    public void setServerPort(Integer serverPort) {
        this.serverPort = serverPort;
    }

    @Override
    public String toString() {
        return "ServerService{" +
                "name='" + name + '\'' +
                ", serverPort=" + serverPort +
                '}';
    }
}
