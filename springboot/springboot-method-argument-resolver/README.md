# 1、SpringBoot中自定义控制层参数解析

## 1、实现步骤

### 1、实现HandlerMethodArgumentResolver接口

自己编写一个类实现`org.springframework.web.method.support.HandlerMethodArgumentResolver`接口

### 2、注入到Spring的上下文中

#### 1、通过WebMvcConfigurer注入

```java
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    
    /**
     * 这个地方加载的顺序是在默认的HandlerMethodArgumentResolver之后的
     */
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        //  resolvers.add(0, new RedisMethodArgumentResolver());
    }
}
```
这种方式加入的，会在Spring提供的默认的参数解析器之后。可以通过打断点`org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.afterPropertiesSet`

#### 2、通过实现BeanPostProcessor

这种方式可以达到在Bean完全实例化之后在对Bean做一些个性化的配置

```java
@Component
static class CustomHandlerMethodArgumentResolverConfig implements BeanPostProcessor {
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof RequestMappingHandlerAdapter) {
            final RequestMappingHandlerAdapter adapter = (RequestMappingHandlerAdapter) bean;
            final List<HandlerMethodArgumentResolver> argumentResolvers = Optional.ofNullable(adapter.getArgumentResolvers())
                    .orElseGet(ArrayList::new);
            final ArrayList<HandlerMethodArgumentResolver> handlerMethodArgumentResolvers = new ArrayList<>(argumentResolvers);
            // 将我们自己的参数解析器放到第一位
            handlerMethodArgumentResolvers.add(0, new RedisMethodArgumentResolver());
            adapter.setArgumentResolvers(Collections.unmodifiableList(handlerMethodArgumentResolvers));
            return adapter;
        }
        return bean;
    }
}
```
