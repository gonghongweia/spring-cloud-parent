package com.huan.study.argument;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author huan.fu 2021/12/7 - 下午3:09
 */
@SpringBootApplication
public class MethodArgumentResolverApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(MethodArgumentResolverApplication.class);
    }
}
