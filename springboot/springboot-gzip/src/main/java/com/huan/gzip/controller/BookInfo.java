package com.huan.gzip.controller;

/**
 * 书籍信息
 *
 * @author huan
 */
public class BookInfo {
    /**
     * 作者
     */
    private String author;
    /**
     * 标题
     */
    private String title;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}