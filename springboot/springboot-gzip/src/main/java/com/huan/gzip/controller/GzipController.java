package com.huan.gzip.controller;

import com.github.javafaker.Book;
import com.github.javafaker.Faker;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 测试 gzip 压缩
 *
 * @author huan.fu
 * @date 2024/3/21 - 16:41
 */
@RequestMapping("gzip")
@RestController
public class GzipController {

    /**
     * 响应内容需要压缩
     */
    @GetMapping("responseCompression")
    public List<BookInfo> responseCompression() {
        return generatorBookInfos();
    }

    /**
     * 生成书籍信息
     *
     * @return List<BookInfo>
     */
    private static List<BookInfo> generatorBookInfos() {
        List<BookInfo> bookInfos = new ArrayList<>(1000);
        Faker faker = Faker.instance();
        for (int i = 0; i < 1000; i++) {
            Book book = faker.book();
            BookInfo bookInfo = new BookInfo();
            bookInfo.setAuthor(book.author());
            bookInfo.setTitle(book.title());
            bookInfos.add(bookInfo);
        }
        return bookInfos;
    }
}
