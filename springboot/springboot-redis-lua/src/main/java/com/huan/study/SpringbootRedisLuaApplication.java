package com.huan.study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * redis 中使用 lua脚本
 */
@SpringBootApplication
public class SpringbootRedisLuaApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(SpringbootRedisLuaApplication.class, args);
    }
}
