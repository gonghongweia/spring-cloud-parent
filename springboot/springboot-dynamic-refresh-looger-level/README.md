# 功能实现

完成在`SpringBoot`环境下动态的刷新系统中的日志级别

# 提供的功能

1. 查看日志级别

```shell
curl http://localhost:8080/getLoggerLevel
```

2. 打印日志

```shell
curl http://localhost:8080/printLog
```

3、设置日志的级别为debug

```shell
curl http://localhost:8080/setLoggerDebugLevel
```

4、设置日志的级别为info

```shell
curl http://localhost:8080/setLoggerInfoLevel
```

