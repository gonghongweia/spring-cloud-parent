package com.huan.study.beandefinition;

import com.huan.study.framewrok.EnableCustomImport;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableCustomImport
public class ImportBeanDefinitionApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImportBeanDefinitionApplication.class, args);
    }
}
