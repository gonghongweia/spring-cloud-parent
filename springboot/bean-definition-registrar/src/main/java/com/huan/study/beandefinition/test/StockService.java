package com.huan.study.beandefinition.test;

import com.huan.study.framewrok.CustomImport;

import java.util.Random;

/**
 * @author huan.fu 2021/4/14 - 上午11:47
 */
@CustomImport(beanName = "stockService")
public class StockService {

    public int stock() {
        return new Random().nextInt(100);
    }
}
