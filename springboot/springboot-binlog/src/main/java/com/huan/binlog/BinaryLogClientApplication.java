package com.huan.binlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 启动类
 * @author huan.fu
 * @date 2024/9/22 - 16:20
 */
@SpringBootApplication
public class BinaryLogClientApplication {

    public static void main(String[] args) {
       //  SpringApplication.run(BinaryLogClientApplication.class, args);

        Date date = new Date(1728713564000L);
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));


    }
}
