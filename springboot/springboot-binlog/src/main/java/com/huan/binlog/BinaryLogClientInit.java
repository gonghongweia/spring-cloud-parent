package com.huan.binlog;

import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.event.*;
import com.github.shyiko.mysql.binlog.event.deserialization.EventDeserializer;
import com.github.shyiko.mysql.binlog.event.deserialization.NullEventDataDeserializer;
import com.huan.binlog.deserializer.CustomUpdateRowsEventDataDeserializer;
import com.huan.binlog.position.FileBinlogPositionHandler;
import com.huan.binlog.utils.DbUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

/**
 * 初始化 binary log client
 *
 * @author huan.fu
 * @date 2024/9/22 - 16:23
 */
@Component
public class BinaryLogClientInit {

    private static final Logger log = LoggerFactory.getLogger(BinaryLogClientInit.class);

    private BinaryLogClient client;

    /**
     * 数据库
     */
    private String database;
    /**
     * 表名
     */
    private String tableName;

    @PostConstruct
    public void init() throws IOException, TimeoutException, NoSuchFieldException, IllegalAccessException {
        /**
         * # 创建用户
         * CREATE USER binlog_user IDENTIFIED BY 'binlog#Replication2024!';
         * GRANT SELECT, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'binlog_user'@'%';
         * FLUSH PRIVILEGES;
         */
        String hostname = "127.0.0.1";
        int port = 3306;
        String username = "binlog_user";
        String password = "binlog#Replication2024!";
        // 创建 BinaryLogClient客户端
        client = new BinaryLogClient(hostname, port, username, password);
        // 这个 serviceId 不可重复
        client.setServerId(12);

        // 反序列化配置
        EventDeserializer eventDeserializer = new EventDeserializer();
        eventDeserializer.setCompatibilityMode(
                // 将日期类型的数据反序列化成Long类型
                EventDeserializer.CompatibilityMode.DATE_AND_TIME_AS_LONG
        );
        // 表示对 删除事件不感兴趣 ( 对于DELETE事件的反序列化直接返回null )
        eventDeserializer.setEventDataDeserializer(EventType.EXT_DELETE_ROWS, new NullEventDataDeserializer());

        Field field = EventDeserializer.class.getDeclaredField("tableMapEventByTableId");
        field.setAccessible(true);
        Map<Long, TableMapEventData> tableMapEventByTableId = (Map<Long, TableMapEventData>) field.get(eventDeserializer);
        eventDeserializer.setEventDataDeserializer(EventType.EXT_UPDATE_ROWS, new CustomUpdateRowsEventDataDeserializer(tableMapEventByTableId)
                .setMayContainExtraInformation(true));

        client.setEventDeserializer(eventDeserializer);

        // 设置 binlog 信息
        FileBinlogPositionHandler fileBinlogPositionHandler = new FileBinlogPositionHandler();
        FileBinlogPositionHandler.BinlogPositionInfo binlogPositionInfo = fileBinlogPositionHandler.retrieveBinlogInfo();
        if (null != binlogPositionInfo) {
            log.info("获取到了binlog 信息 binlogName: {} position: {} serverId: {}", binlogPositionInfo.binlogName,
                    binlogPositionInfo.position, binlogPositionInfo.serverId);
            client.setBinlogFilename(binlogPositionInfo.binlogName);
            client.setBinlogPosition(binlogPositionInfo.position);
            client.setServerId(binlogPositionInfo.serverId);
        }

        client.registerEventListener(new BinaryLogClient.EventListener() {
            @Override
            public void onEvent(Event event) {
                EventType eventType = event.getHeader().getEventType();
                log.info("接收到事件类型: {}", eventType);
                log.warn("接收到的完整事件: {}", event);
                log.info("============================");

                // FORMAT_DESCRIPTION（写入每个二进制日志文件前的描述事件） HEARTBEAT（心跳事件）这2个事件不进行binlog位置的记录
                if (eventType != EventType.FORMAT_DESCRIPTION && eventType != EventType.HEARTBEAT) {
                    // 当有binlog文件切换时产生
                    if (event.getData() instanceof RotateEventData) {
                        RotateEventData eventData = event.getData();
                        // 保存binlog position 信息
                        fileBinlogPositionHandler.saveBinlogInfo(eventData.getBinlogFilename(), eventData.getBinlogPosition(), event.getHeader().getServerId());
                    } else {
                        // 非 rotate 事件，保存位置信息
                        EventHeaderV4 header = event.getHeader();
                        FileBinlogPositionHandler.BinlogPositionInfo info = fileBinlogPositionHandler.retrieveBinlogInfo();
                        long position = header.getPosition();
                        long serverId = header.getServerId();
                        fileBinlogPositionHandler.saveBinlogInfo(info.binlogName, position, serverId);
                    }
                }


                // 通过 TableMap 事件获取 数据库名和表名
                if (event.getData() instanceof TableMapEventData) {
                    TableMapEventData eventData = (TableMapEventData) event.getData();
                    database = eventData.getDatabase();
                    tableName = eventData.getTable();
                    log.info("获取到的数据库名: {} 和 表名为: {}", database, tableName);
                }

                // 监听更新事件
                if (event.getData() instanceof UpdateRowsEventData) {
                    try {
                        // 获取表的列信息
                        Map<String, String> columnInfo = DbUtils.retrieveTableColumnInfo(database, tableName);
                        // 获取更新后的数据
                        UpdateRowsEventData eventData = ((UpdateRowsEventData) event.getData());
                        // 可能更新多行数据
                        List<Map.Entry<Serializable[], Serializable[]>> rows = eventData.getRows();

                        for (Map.Entry<Serializable[], Serializable[]> row : rows) {
                            // 更新前的数据
                            Serializable[] before = row.getKey();
                            // 更新后的数据
                            Serializable[] after = row.getValue();
                            // 保存更新后的一行数据
                            Map<String, Serializable> afterUpdateRowMap = new HashMap<>();
                            for (int i = 0; i < after.length; i++) {
                                // 因为 columnInfo 中的列名的位置是从1开始，而此处是从0开始
                                afterUpdateRowMap.put(columnInfo.get((i + 1) + ""), after[i]);
                            }
                            log.info("监听到更新的数据为: {}", afterUpdateRowMap);
                        }
                    } catch (Exception e) {
                        log.error("监听更新事件发生了异常");
                    }
                }

                // 监听插入事件
                if (event.getData() instanceof WriteRowsEventData) {
                    log.info("监听到插入事件");
                }

                // 监听删除事件
                if (event.getData() instanceof DeleteRowsEventData) {
                    log.info("监听到删除事件");
                }
            }
        });
        client.registerLifecycleListener(new BinaryLogClient.AbstractLifecycleListener() {
            @Override
            public void onConnect(BinaryLogClient client) {
                log.info("客户端连接到 mysql 服务器 client: {}", client);
            }

            @Override
            public void onCommunicationFailure(BinaryLogClient client, Exception ex) {
                log.info("客户端和 mysql 服务器 通讯失败 client: {}", client);
            }

            @Override
            public void onEventDeserializationFailure(BinaryLogClient client, Exception ex) {
                log.info("客户端序列化失败 client: {}", client);
            }

            @Override
            public void onDisconnect(BinaryLogClient client) {
                log.info("客户端断开 mysql 服务器链接 client: {}", client);
            }
        });
        // client.connect 在当前线程中进行解析binlog，会阻塞当前线程
        // client.connect(xxx) 会新开启一个线程，然后在这个线程中解析binlog
        client.connect(10000);
    }

    @PreDestroy
    public void destroy() throws IOException {
        client.disconnect();
    }
}
