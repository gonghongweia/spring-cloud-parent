package com.huan.springcloud.product;

import com.huan.springcloud.common.CommonApi;
import org.springframework.context.annotation.Bean;

/**
 * 不可使用 @Configuration 注解
 *
 * @author huan.fu
 * @date 2023/8/15 - 10:05
 */
public class ProductApiConfig {

    @Bean
    public CommonApi commonApi() {
        return new ProductApi();
    }
}
