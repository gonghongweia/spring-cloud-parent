package com.huan.study.redis.controller;

import com.huan.study.redis.constan.Cosntants;
import com.huan.study.redis.stream.producer.StreamProducer;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 发送消息到Stream
 *
 * @author huan.fu 2021/11/11 - 下午5:56
 */
@RestController
@AllArgsConstructor
public class ProducerController {
    
    private final StreamProducer streamProducer;
    
    @GetMapping("sendBookToStream")
    public String sendBookToStream() {
        streamProducer.sendRecord(Cosntants.STREAM_KEY_001);
        return "ok";
    }
}
