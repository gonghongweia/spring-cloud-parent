package com.huan.api

/**
 * 泛型的使用
 * @author huan.fu
 * @date 2024/6/23 - 14:42
 */
class 泛型 {
    static void main(String[] args) {
        KeyValue<String, Integer> keyValue = new KeyValue<>("String Value", 12)
        println keyValue
    }
}


class KeyValue<T, V> {
    private T key
    private V value

    KeyValue(T key, V value) {
        this.key = key
        this.value = value
    }

    @Override
    String toString() {
        return "KeyValue{" +
                "key=" + key +
                ", value=" + value +
                '}';
    }
}