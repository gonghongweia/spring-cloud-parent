package com.huan.api

/**
 * 异常处理
 *
 * @author huan.fu
 * @date 2024/6/23 - 14:35
 */
public class 异常处理 {
    static def div(int i, int j) {
        try {
            int result = i / j
            println result
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    static void main(String[] args) {
        div(1, 0)
    }

}

/**
 * 自定义一个异常
 */
class ServiceException extends RuntimeException {
    public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    protected ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
