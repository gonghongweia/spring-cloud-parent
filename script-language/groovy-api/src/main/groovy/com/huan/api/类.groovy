package com.huan.api

/**
 * 面向对象类
 * @author huan.fu
 * @date 2024/6/22 - 23:34
 */
class 类 {

    private int id;

    static void main(String[] args) {
        Outer outer = new Outer();
        outer.name = "张三";
        outer.callInnerClassMethod();
    }

    int getId() {
        return id
    }

    void setId(int id) {
        this.id = id
    }
}


// 外层类
class Outer {
    String name;

    def callInnerClassMethod() {
        // 调用内部类的方法
        new Inner().innerMethod();
    }

    // 内部类
    class Inner {
        def innerMethod() {
            println "这是内部类的方法,调用外部类的字段 $name"
        }
    }
}