package com.huan.api

/**
 * 闭包的简单使用
 * @author huan.fu
 * @date 2024/6/23 - 16:36
 */
class 闭包 {


    static def show(param1, param2, block) {
        block.call(param1, param2)
    }

    static void main(String[] args) {
        show("参数1", "参数2", { param1, param2 -> println " $param1, $param2" })

        def arr = [1, 2, 3, 4, 5];
        arr.each { it ->
            if (it % 2 == 0) {
                println it
            }
        }

        def block = { Integer param1, String param2 ->
            if (param1 % 2 == 0) {
                println "$param1 ==> $param2"
            } else {
                println "$param2"
            }
        }
        block.call(4, "张三")
    }
}
