package com.huan.api

/**
 * 数值类型分为两类，包括整数型(Integer)和小数型(decimal)，整数型包括以下几种
 * byte、char、short、int、long、java.math.BigInteger
 * @author huan.fu
 * @date 2024/6/18 - 23:41
 */
class 数值类型_整形 {
    static void main(String[] args) {

        println("======== 定义数值型 ============")
        def a = 1 // 整型
        assert a instanceof Integer

        def b = 2147483647 // Integer类型的最大值
        assert b instanceof Integer

        def c = 2147483648 // Integer类型的最大值+1
        assert c instanceof Long

        def d = Long.MAX_VALUE // Long类型的最大值
        assert d instanceof Long
        println d
        def e = 9223372036854775808 // Long 类型的最大值+1
        assert e instanceof BigInteger


        println("======== 定义二进制型，以0b为前缀 ============")
        int xInt = 0b10101111
        assert xInt == 175

        println("======== 定义八进制型，以0为前缀 ============")
        int yInt = 077
        assert yInt == 63

        println("======== 定义十六进制型，以0x为前缀 ============")
        int zInt = 0x77
        assert zInt == 119
    }
}
