package com.huan.api

/**
 * 字符串
 * @author huan.fu
 * @date 2024/6/18 - 23:21
 */
class 字符串 {

    static void main(String[] args) {
        println '单引号字符串'
        println "双引号字符串"
        println '''3个单引号字符串 '''
        println """3个双引号字符串 """
        def name = "张三";

        println("=================字符串中带变量=================")

        println '字符串中带变量 ${name}'
        println "字符串中带变量 ${name}"
        println '''字符串中带变量 ${name}'''
        println """
                    字符串中带变量 
                        ${name}
                """

        println(name.find())

    }
}
