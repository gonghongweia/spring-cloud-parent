package com.huan.api

/**
 * groovy中的集合使用的是java中的集合，数组和java中的数组一样
 * @author huan.fu
 * @date 2024/6/19 - 00:01
 */
class 集合类和数组 {

    static void main(String[] args) {
        // 默认是ArrayList类型
        def arr = [1, 2, 3]
        assert arr instanceof ArrayList

        // as 定义为想要的类型
        def arr1 = [1, "aa", true] as LinkedList

        println(arr1[0])
        println(arr1[-1])
        println("arr1 size: " + arr1.size())

        arr1 << "在集合的末尾增加元素"
        println arr1[3]

        // 定义数据
        String[] arr2 = ["aa", "bb"];
        assert arr2 instanceof String[];
        def arr3 = new String[]{1, 2, 3, 4, 5}
        println arr3[0]

    }

}
