package com.huan.api

/**
 * Trait和interface很想，但是Trait更强大
 * 行为组成。
 * 接口的运行时实现。
 * 行为覆盖。
 * 兼容静态类型检查/编译
 * @author huan.fu
 * @date 2024/6/23 - 15:32
 */
class Trait特征 {
    static void main(String[] args) {
        new AliyunEmailProvider().send("aa", "bb");
    }
}

trait EmailProvider {
/**
 * 属性
 */
    def provider = "default";

    /**
     * 私有方法
     */
    private def privateMethod() {
        println "这是私有方法"
    }

    /**
     * 默认实现
     */
    def send(String from, String to) {
        println "默认实现"
        doSend(from, to)
    }

    /**
     * 抽象方法
     */
    abstract doSend(String from, String to);
}

trait ShowName {
    def show() {
        println "展示"
    }
}

class AliyunEmailProvider implements ShowName, EmailProvider {

    @Override
    def doSend(String from, String to) {
        println "aliyun from: $from to:${to}"
    }
}