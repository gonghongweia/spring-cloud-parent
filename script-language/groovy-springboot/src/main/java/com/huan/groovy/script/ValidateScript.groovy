package com.huan.groovy.script;

import com.huan.groovy.vo.AddUserRequest;
import org.apache.commons.lang3.StringUtils;

/**
 * 验证脚本
 * @author huan.fu
 * @date 2024/7/17 - 12:37
 */
class ValidateScript {

    <T> T invoked(Map<Object, Object> content) {
        def tempList = new ArrayList<>();
        for (i in 0..<1000000) {
            tempList.add(i);
        }

        // 1、获取到请求参数
        AddUserRequest param = content["param"] as AddUserRequest;
        // 2、获取UserService类
        def userService = content["userService"];
        // 3、进行参数验证
        if (userService.existUserName(param.getUsername())) {
            return "用户名 " + param.getUsername() + " 已存在" as T;
        }
        if (param.getSex() != 0 && param.getSex() != 1) {
            return "sex的值只可以是 0和1" as T;
        }
        if (StringUtils.isBlank(param.getProvince())) {
            return "省不可为空";
        }

        return "添加成功" as T;
    }
}
