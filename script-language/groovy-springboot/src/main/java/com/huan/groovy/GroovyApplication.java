package com.huan.groovy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot 集成 groovy
 *
 * @author huan.fu
 * @date 2024/7/16 - 21:36
 */
@SpringBootApplication
public class GroovyApplication {

    public static void main(String[] args) {
        SpringApplication.run(GroovyApplication.class, args);
    }
}
