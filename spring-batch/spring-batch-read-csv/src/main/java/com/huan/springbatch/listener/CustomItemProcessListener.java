package com.huan.springbatch.listener;

import com.huan.springbatch.dto.Person;
import org.springframework.batch.core.ItemProcessListener;

/**
 * ItemProcessListener和ItemReadListener类似，是围绕着ItemProcessor进行处理的
 *
 * @author huan.fu
 * @date 2022/8/29 - 23:02
 */
public class CustomItemProcessListener implements ItemProcessListener<Person, Person> {
    @Override
    public void beforeProcess(Person item) {
        // processor执行之前
    }

    @Override
    public void afterProcess(Person item, Person result) {
        // processor执行成功之后
    }

    @Override
    public void onProcessError(Person item, Exception e) {
        // processor执行出现异常
    }
}
