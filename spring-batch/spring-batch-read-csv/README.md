# 需求

每日从指定日期的文件夹中，读取csv文件，并处理

# 实现步骤

## 1、获取具体的文件路径

此处通过 `AssemblyReadCsvPathListener` 来获取，并放入到 `ExecutionContext` 中，ExecutionContext中不要放入比较大的数据，因为这个数据会
存入到数据库中

## 2、打印job参数和execution context中的参数

此处通过 `PrintImportFilePathTaskLet` 来实现

## 3、读取并处理数据

此处需要注意，如果用的是 `FlatFileItemReader` 类型，则需要返回FlatFileItemReader类型， 而不要返回ItemReader
否则可能报如下异常 `Reader must be open before it can be read`

## 4、如何获取到ExecutionContext中的参数

1. 类上加入 `@StepScope` 注解
2. 通过 `@Value("#{jobExecutionContext['importPath']}")` 来获取

## 5、手动启动job

`spring.batch.job.enabled=false`

## 6、 chunk=2 ItemReader 读取数据给 ItemProcess，当读取数量到达chunk时，交给ItemWriter

`@EnableBatchProcessing` 启用 Spring Batch的自动配置