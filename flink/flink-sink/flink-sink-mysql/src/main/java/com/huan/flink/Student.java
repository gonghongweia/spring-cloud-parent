package com.huan.flink;

/**
 * 学生类
 *
 * @author huan.fu
 * @date 2023/9/23 - 15:07
 */
public class Student {
    /**
     * 学生id
     */
    private Integer studentId;
    /**
     * 学生姓名
     */
    private String studentName;

    public Student(Integer studentId, String studentName) {
        this.studentId = studentId;
        this.studentName = studentName;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
}
