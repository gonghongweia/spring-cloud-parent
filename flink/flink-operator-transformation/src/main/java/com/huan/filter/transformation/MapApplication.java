package com.huan.filter.transformation;

import com.huan.filter.vo.Person;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * map 算子： 数据转换，一一转换
 * 需求： map转换 Person转换成Name, 输出用户名
 *
 * @author huan.fu
 * @date 2023/9/18 - 22:43
 */
public class MapApplication {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();

        environment.fromElements(
                        new Person(1, "张三", 20),
                        new Person(2, "李四", 25),
                        new Person(3, "王五", 30)
                )
                // map 转换
                .map((MapFunction<Person, String>) Person::getName)
                .print();

        environment.execute("map operation");
    }
}