# flink 程序打包步骤

1. 针对 flink 的包，我们在`maven`中将作用域修改成`provided`
   ![img.png](flink java包作用域修改成provided后 如何运行.png)
2. 引入打包插件 `maven-shade-plugin`, 使用该插件时先 `clean package`
