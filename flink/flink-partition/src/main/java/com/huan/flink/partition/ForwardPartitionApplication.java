package com.huan.flink.partition;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * one-to-one 下游算子的并行度必须是1
 *
 * @author huan.fu
 * @date 2023/9/23 - 06:51
 */
public class ForwardPartitionApplication {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        // 设置并行度为2
        environment.setParallelism(2);

        environment.fromElements(
                        new Student(1L, 1L, "张三"),
                        new Student(2L, 1L, "张三"),
                        new Student(3L, 2L, "张三"),
                        new Student(4L, 2L, "张三"),
                        new Student(5L, 2L, "张三"),
                        new Student(6L, 3L, "张三")
                )
                // one-to-one 下游算子的并行度必须是1
                .forward()
                .print()
                // 此处算子的并行度必须是1
                .setParallelism(1);

        environment.execute("forward job");

    }
}
