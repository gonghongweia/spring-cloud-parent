package com.huan.flink.partition;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * keyBy 根据某个key的值进行分区，此处根据classId进行分区，相同的classId的值进入到同一个分区中，即由同一个子任务进行处理
 *
 * @author huan.fu
 * @date 2023/9/23 - 06:51
 */
public class KeyByPartitionApplication {
    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        // 设置并行度为2
        environment.setParallelism(2);

        environment.fromElements(
                        new Student(1L, 1L, "张三"),
                        new Student(2L, 1L, "张三"),
                        new Student(3L, 2L, "张三"),
                        new Student(4L, 2L, "张三"),
                        new Student(5L, 2L, "张三"),
                        new Student(6L, 3L, "张三")
                )
                // keyBy 根据某个key的值进行分区，此处根据classId进行分区，相同的classId的值进入到同一个分区中，即由同一个子任务进行处理
                .keyBy(Student::getClassId)
                .print();

        environment.execute("keyBy job");

    }
}
