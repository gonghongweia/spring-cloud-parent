package com.huan.flink.map;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.flink.api.common.functions.MapFunction;

/**
 * @author huan.fu
 * @date 2024/1/6 - 11:03
 */
public class ProductMapFunction implements MapFunction<String, Product> {
    @Override
    public Product map(String value) throws Exception {
        String[] arr = value.split(",");
        Product product = new Product();
        product.setProductId(Integer.parseInt(arr[0]));
        product.setProductName(arr[1]);
        product.setCreateTime(DateUtils.parseDateStrictly(arr[2], "yyyy-MM-dd HH:mm:ss"));
        return product;
    }
}
