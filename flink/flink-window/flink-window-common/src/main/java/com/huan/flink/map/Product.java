package com.huan.flink.map;

import lombok.Data;

import java.util.Date;

/**
 * 商品
 *
 * @author huan.fu
 * @date 2024/1/6 - 11:04
 */
@Data
public class Product {
    /**
     * 商品id
     */
    private Integer productId;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 创建时间
     */
    private Date createTime;
}
