# 分流

需求：根据数据的type进行判断，转换成不同的对象，分别做处理

## filter 实现

```java
/**
 * 需求： 根据数据的type进行判断，转换成不同的对象，分别做处理
 * 实现： 此处是通过filter来实现的，但是这样虽然实现了功能，但不是最好的，因为 同一份数据，需要经过多次过滤处理，浪费资源。假设在filter()方法
 * 中需要调用数据库获取数据，然后进行判断，那么在这个需求中，同一条数据，需要查询3次数据库，因为有3个filter方法
 *
 * @author huan.fu
 * @date 2023/9/23 - 10:01
 */
public class FilterStreamApplication {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();

        SingleOutputStreamOperator<Animal> ds = environment.fromElements(
                        new Animal("1", "鸭子"),
                        new Animal("1", "小鸭子"),
                        new Animal("2", "三黄鸡"),
                        new Animal("2", "大公鸡"),
                        new Animal("3", "其他动物")
                )
                .map(a -> a);

        // 过滤出鸭子进行操作、输出
        ds.filter(animal -> Objects.equals("1", animal.getType()))
                .map(animal -> new Duck(animal.getType(), animal.getName()))
                .print();
        // 过滤出鸡进行操作、输出
        ds.filter(animal -> Objects.equals("2", animal.getType()))
                .map(animal -> new Chicken(animal.getType(), animal.getName()))
                .print();
        // 过滤出其他进行操作、输出
        ds.filter(animal -> Objects.equals("3", animal.getType()))
                .print();


        environment.execute("分流");
    }
}
```

## side output 侧输出流

```java
/**
 * 需求： 根据数据的type进行判断，转换成不同的对象，分别做处理
 * 实现：通过 side output 侧输出流来实现
 *
 * @author huan.fu
 * @date 2023/9/23 - 10:01
 */
public class SideOutputStreamApplication {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();

        SingleOutputStreamOperator<Animal> ds = environment.fromElements(
                        new Animal("1", "鸭子"),
                        new Animal("1", "小鸭子"),
                        new Animal("2", "三黄鸡"),
                        new Animal("2", "大公鸡"),
                        new Animal("3", "其他动物")
                )
                .map(a -> a);

        /**
         * 定义一个侧输出流标签
         * 第一个参数：标签名
         * 第二个参数： 放入侧输出流中的数据的数据类型
         */
        OutputTag<Duck> duckOutputTag = new OutputTag<>("duck", Types.POJO(Duck.class));
        OutputTag<Chicken> chickenOutputTag = new OutputTag<>("chicken", Types.POJO(Chicken.class));

        /**
         * process方法参数
         * 第一个参数：ProcessFunction 处理方法
         * 第二个参数： 主流中的数据类型
         */
        SingleOutputStreamOperator<Animal> os = ds.process(new ProcessFunction<Animal, Animal>() {
            @Override
            public void processElement(Animal animal, ProcessFunction<Animal, Animal>.Context ctx, Collector<Animal> out) throws Exception {
                switch (animal.getType()) {
                    case "1":
                        Duck duck = new Duck(animal.getType(), animal.getName());
                        // 将 Duck 对象放入 侧输出流中
                        ctx.output(duckOutputTag, duck);
                        break;
                    case "2":
                        Chicken chicken = new Chicken(animal.getType(), animal.getName());
                        // 将 Chicken 对象放入侧输出流中
                        ctx.output(chickenOutputTag, chicken);
                        break;
                    default:
                        // 将 Animal 对象放入主流中
                        out.collect(animal);
                        break;
                }
            }
        }, Types.POJO(Animal.class));

        // 获取Duck侧输出流
        SideOutputDataStream<Duck> duckOs = os.getSideOutput(duckOutputTag);
        // 获取Chicken侧输出流
        SideOutputDataStream<Chicken> chickenOs = os.getSideOutput(chickenOutputTag);

        duckOs.print("duck 侧输出流");
        chickenOs.print("chicken 侧输出流");
        os.print("主流");

        environment.execute("分流");
    }
}
```
