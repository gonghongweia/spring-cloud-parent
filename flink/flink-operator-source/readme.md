# 1. 从集合中读取数据
```java
/**
 * 从集合中读取数据
 */
environment.fromElements("hello", "java", "flink");
environment.fromCollection(Arrays.asList("hello", "java", "flink"));
```

# 2. 从文件中读取数据

## 1、导入依赖

```xml
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-connector-files</artifactId>
    <version>1.17.1</version>
</dependency>
```

## 2、从文件中读取

```java
// 构建FileSource
FileSource<String> fileSource = FileSource.forRecordStreamFormat(new TextLineInputFormat(), Path.fromLocalFile(new File(filePath))).build();
// 名字，随便写，有意义即可
String sourceName = "fileSource";
// 新的Source写法
environment.fromSource(fileSource, WatermarkStrategy.noWatermarks(), sourceName)
```

# 3、从socket中读取数据

```java
environment.socketTextStream("localhost", 9999)
```

# 4、自动生成数据

## 4.1 引入依赖
```xml
<dependency>
    <groupId>org.apache.flink</groupId>
    <artifactId>flink-connector-datagen</artifactId>
    <version>1.17.1</version>
</dependency>
```

## 4.2 生成数据

```java
public class FlinkGeneratorSourceApplication {

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();

        // 自动生成数据
        DataGeneratorSource<String> source = new DataGeneratorSource<>(
                // 生成数据
                (GeneratorFunction<Long, String>) value -> LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + " generator value: " + value,
                // 生成多少个数量
                100,
                // 每秒生成多少个数据,是设置所有的subTask每秒产生1个，而不是每个subTask每秒产生1个
                RateLimiterStrategy.perSecond(1),
                // 生成的数据经过 map 之后是什么类型
                Types.STRING
        );
        String sourceName = "datagen";
        environment.fromSource(source, WatermarkStrategy.noWatermarks(), sourceName)
                // 设置并行度为2，上述datagen的count=100,则每个并行度生成 count(100)/parallelism(2)=50个数据，其中一个是[0-49],另一个是[50-99]
                .setParallelism(2)
                .print();

        environment.execute("generator-source");
    }
}
```

# 注意实现

因为`pom.xml`中的`flink`相关的依赖为`provided`，所以在idea中运行，程序可能报找不到类，查看 [flink/flink-build-package/readme.md](flink/flink-build-package/readme.md)文件解决