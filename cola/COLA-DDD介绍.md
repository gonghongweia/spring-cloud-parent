大部分内容都是借鉴网上的文章。

# 一、包结构介绍

| 层次          | 包名                | 功能                    |
|-------------|-------------------|-----------------------|
| Adapter层    | web               | 处理页面请求的Controller     |
| Adapter层    | wireless          | 处理无线端的适配              |
| Adapter层    | wap               | 处理wap端的适配             |
| App层        | executor.command  | CmdExe执行器             |
| App层        | executor.query    | QryExe执行器             |
| App层        | extensionpoint    | 扩展点接口                 |
| App层        | extension         | 扩展点实现                 |
| App层        | interceptor       | 各种拦截器 @XxxInterceptor |
| App层        | serviceimpl       | service实现             |
| App层        | event.handler     | 处理外部事件                |
| App层        | scheduler         | 处理定时任务                |
| App层        | assembler         | 参数装配器                 |
| Domain层     | model.aggregate   | 聚合根类                  |
| Domain层     | model.entities    | 实体类                   |
| Domain层     | model.valueobject | 值对象                   |
| Domain层     | ability           | 领域能力，DomainService接口  |
| Domain层     | ability.impl      | 领域能力，DomainService实现  |
| Domain层     | gateway           | 领域网关，解耦利器             |
| Infra层      | gatewayimpl       | 网关实现                  |
| Infra层      | mapper            | ibatis数据库映射           |
| Infra层      | config            | 配置信息                  |
| Infra层      | convertor         | 领域模型和数据库对象之间的转换工具类    |
| Infra层      | event             | 领域事件技术实现类             |
| Infra层      | rpc               | rpc实现                 |
| Client SDK  | api               | 服务对外透出的API            |
| Client SDK  | dto               | 服务对外的DTO              |
| Client SDK  | dto.cmd           | 服务对外CQRS中的CUD操作的请求对象  |
| Client SDK  | dto.qry           | 服务对外CQRS中的R操作的请求对象    |
| Client SDK  | dto.co            | 服务对外返回的客户端对象          |
| Client SDK  | dto.event         | 服务对外的事件               |

# 二、各层介绍

1、适配层 `Adapter`： 负责进行路由和适配。简单的理解就是控制层。  
2、应用层 `Application`： 负责获取输入、组装上下文参数、做输入校验、发送消息给领域层做业务处理同时负责监听领域事件消息。采用CQRS
架构模型，查询和命令分离，同时，查询和命令执行器(executor)也是在这层实现的。`事物Transaction`也是在这层实现的。  
3、领域层 `Domain`：领域应用的核心，主要封装了核心业务逻辑，通过领域服务、领域对象的方法对应用层提供业务实体和业务逻辑计算。  
4、基础设施层 `Infrastructure`：主要负责技术细节的实现，比如数据库的CRUD、搜索引擎Elasticsearch、外部服务等。在通过RPC调用
外部服务时，`防腐层`设计也位于该层，进行转义处理，隔离限界上下文之间的耦合，实现干净的领域模型。

`Application` 层，负责编排业务，而不负责业务逻辑。  
`Domain Service`层，负责编排领域。  

# 三、业务调用时序图

## 1、Command或Query执行器直接调用Gateway接口处理业务

![Command或Query执行器直接调用Gateway接口处理业务](./images/1.jpg)

## 2、Command或Query执行器调用领域服务Domain Service处理业务

![Command或Query执行器调用领域服务Domain Service处理业务](./images/2.jpg)

## 3、Command或Query直接调用Infrastructure处理业务

![Command或Query直接调用Infrastructure处理业务](./images/3.jpg)

# 四、聚合的作用

举个例子。社会是由一个个的个体组成的，象征着我们每一个人。随着社会的发展，慢慢出现了社团、机构、部门等组织，我们开始从个人变成了组织的一员，大家可以协同一致的工作，朝着一个最大的目标前进，发挥出更大的力量。

领域模型内的实体和值对象就好比个体，而能让实体和值对象协同工作的组织就是聚合，它用来确保这些领域对象在实现共同的业务逻辑时，能保证数据的一致性。

你可以这么理解，聚合就是由业务和逻辑紧密关联的实体和值对象组合而成的，聚合是数据修改和持久化的基本单元，每一个聚合对应一个仓储，实现数据的持久化。

聚合有一个聚合根和上下文边界，这个边界根据业务单一职责和高内聚原则，定义了聚合内部应该包含哪些实体和值对象，而聚合之间的边界是松耦合的。按照这种方式设计出来的微服务很自然就是“高内聚、低耦合”的。

`聚合`在 `DDD` 分层架构里属于`领域层`，领域层包含了多个聚合，共同实现核心业务逻辑。  
1. 聚合内实体以充血模型实现个体业务能力，以及业务逻辑的高内聚。  
2. 跨多个实体的业务逻辑通过领域服务来实现，跨多个聚合的业务逻辑通过应用服务来实现。  
3. 比如有的业务场景需要同一个聚合的 A 和 B 两个实体来共同完成，我们就可以将这段业务逻辑用`领域服务来实现`；  
4. 而有的业务逻辑需要聚合 C 和聚合 D 中的两个服务共同完成，这时你就可以用`应用服务来组合这两个服务`。

参考链接: [https://www.cnblogs.com/junzi2099/p/13682086.html](https://www.cnblogs.com/junzi2099/p/13682086.html)
