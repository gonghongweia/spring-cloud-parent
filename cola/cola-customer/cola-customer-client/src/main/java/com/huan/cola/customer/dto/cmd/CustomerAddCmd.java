package com.huan.cola.customer.dto.cmd;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class CustomerAddCmd {

    @NotBlank(message = "手机号不可为空")
    private String phone;
    @NotBlank(message = "身份证不可为空")
    private String idCard;
    private String address;
    @NotNull(message = "客户类型不可为空")
    private Integer customerType;
}
