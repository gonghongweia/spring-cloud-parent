package com.huan.cola.customer.event.handler;

import com.huan.cola.customer.dto.event.CustomerCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * 处理客户创建后的领域事件
 *
 * @author huan.fu
 * @date 2022/6/7 - 17:58
 */
@Component
public class CustomerCreateEventHandler {

    private static final Logger log = LoggerFactory.getLogger(CustomerCreateEventHandler.class);

    @EventListener
    public void handler(CustomerCreateEvent event) {
        log.info("处理客户成功创建事件, customerId: {}", event.getCustomerId());
    }
}
