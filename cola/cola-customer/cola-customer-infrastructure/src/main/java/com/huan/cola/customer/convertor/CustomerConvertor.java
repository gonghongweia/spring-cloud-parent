package com.huan.cola.customer.convertor;

import com.huan.cola.customer.domain.model.Customer;
import com.huan.cola.customer.domain.model.vauleobject.CustomerType;
import com.huan.cola.customer.dto.cmd.CustomerAddCmd;
import com.huan.cola.customer.gatewayimpl.database.dataobject.CustomerDO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author huan.fu
 * @date 2022/6/7 - 11:28
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CustomerConvertor {

    public static Customer toDomainObject(CustomerDO customerDO) {
        Customer customer = new Customer();
        customer.setId(customerDO.getId());
        customer.setPhone(customerDO.getPhone());
        customer.setAddress(customerDO.getAddress());
        customer.setIdCard(customerDO.getIdCard());
        customer.setCustomerType(CustomerType.of(customerDO.getCustomerType()));
        return customer;
    }

    public static Customer toDomainObject(CustomerAddCmd cmd) {
        Customer customer = new Customer();
        customer.setPhone(cmd.getPhone());
        customer.setAddress(cmd.getAddress());
        customer.setIdCard(cmd.getIdCard());
        customer.setCustomerType(CustomerType.of(cmd.getCustomerType()));
        return customer;
    }

    public static CustomerDO toDataObject(Customer customer) {
        CustomerDO customerDO = new CustomerDO();
        customerDO.setId(customer.getId());
        customerDO.setPhone(customer.getPhone());
        customerDO.setAddress(customer.getAddress());
        customerDO.setIdCard(customer.getIdCard());
        customerDO.setCustomerType(customer.getCustomerType().getType());
        return customerDO;
    }
}
