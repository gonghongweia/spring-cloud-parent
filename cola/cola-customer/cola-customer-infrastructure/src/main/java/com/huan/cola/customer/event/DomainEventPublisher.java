package com.huan.cola.customer.event;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 发布领域事件
 *
 * @author huan.fu
 * @date 2022/6/7 - 17:48
 */
@Component
public class DomainEventPublisher {

    @Resource
    private ApplicationEventPublisher applicationEventPublisher;

    public void publisher(Object event) {
        applicationEventPublisher.publishEvent(event);
    }
}
