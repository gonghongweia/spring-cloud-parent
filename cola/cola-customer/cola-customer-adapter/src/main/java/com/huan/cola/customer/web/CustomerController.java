package com.huan.cola.customer.web;

import com.alibaba.cola.dto.MultiResponse;
import com.alibaba.cola.dto.SingleResponse;
import com.huan.cola.customer.api.CustomerService;
import com.huan.cola.customer.dto.cmd.CustomerAddCmd;
import com.huan.cola.customer.dto.data.CustomerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 客户控制器
 */
@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("findAll")
    public MultiResponse<CustomerDTO> findAll() {
        return customerService.findAll();
    }

    @PostMapping("create")
    public SingleResponse<Boolean> create(@RequestBody @Valid CustomerAddCmd customerAddCmd) {
        return customerService.addCustomer(customerAddCmd);
    }
}
